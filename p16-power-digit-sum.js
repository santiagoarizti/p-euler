//const pow2 = 2n ** 15n;
// I found that using bigint I can easily get the dec version of the number I am looking for!
// who knows how bigint works though!
const pow2 = 2n ** 1000n;
const s = `${pow2}`;
let sum = 0;
for (let i = 0; i < s.length; i++) {
    sum += parseInt(s[i]);
}

console.log(s, sum);
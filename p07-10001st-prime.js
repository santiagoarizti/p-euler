const num = 10001;

// is-prime function uses cache to avoid calculating double. it is recursive.
const checkedPrimes = [];
function isPrime(n) {
    if (checkedPrimes[n] !== undefined) {
        // if we calculated it before, reuse that result.
        return checkedPrimes[n];
    }
    
    let p = true;
    // because symetry of order of factors in product, we don't need to explore above sqrt
    const sqn = Math.sqrt(n);
    for (let i = Math.floor(sqn); i > 1; i--) {
        if (isPrime(i)) {
            if (n % i === 0) {
                p = false;
                break;
            }    
        }
    }

    checkedPrimes[n] = p;
    return p;
}

let count = 0;
for (let i = 2; true; i++) {
    if (isPrime(i)) {
        count++;
        if (count === num) {
            console.log(i);
            break;
        }
    }
}
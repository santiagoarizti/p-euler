// You are given the following information, but you may prefer to do some research for yourself.
// 
// 1 Jan 1900 was a Monday.
// Thirty days has September,
// April, June and November.
// All the rest have thirty-one,
// Saving February alone,
// Which has twenty-eight, rain or shine.
// And on leap years, twenty-nine.
// A leap year occurs on any year evenly divisible by 4, but not on a century unless it is divisible by 400.
// How many Sundays fell on the first of the month during the twentieth century (1 Jan 1901 to 31 Dec 2000)?

let d = new Date(1901, 1-1, 1);
const maxDate = new Date(2000, 12-1, 31);
let count = 0;
for (let d = new Date(1901, 1-1, 1); d <= maxDate; d.setDate(d.getDate()+1)) {
    if (d.getDate() === 1 && d.getDay() === 0) {
        //console.log(d.toString());
        count++;
    }
}
console.log("Using date", count);

function getDaysInMonth(y, m) {
    return m % 2 === 0 
        ? /* jan, etc.*/ 31 
        : /* feb, etc.*/ (
            m !== 1 ? 30
            : (y % 400 === 0 || y % 4 === 0 && y % 100 !== 0 ? 29 : 28)
        );
}

// ok, now with own algorithm
count  = 0;
let y = 1900;
let m = 0;
d = 1;
let w = 1;

while (y < 2001) {
    if (d === 1 && w === 0) {
        count++;
    }
    d++;
    w = (w + 1) % 7;
    const ml = getDaysInMonth(y, m);
    if (d > ml) {
        d = 1;
        m++;
    }
    if (m > 11) {
        m = 0;
        y++;
    }
}
console.log("by hand", count);

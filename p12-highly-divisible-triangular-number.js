const num = 500;
let t = 0;
for (let i = 1; true; i ++) {
    t += i;
    const f = getFactors(t);
    if (f.length + 2 > num) {
        console.log(t);
        break;
    }
}

function getFactors(n) {
    const f = [];
    const sqn = Math.sqrt(n);
    for (let i = 2; i < sqn; i++) {
        if (n % i === 0) f.push(i, n / i);
    }
    return f;
}

/*
A unit fraction contains 1 in the numerator. The decimal representation of the unit fractions with denominators 2 to 10 are given:

1/2	= 	0.5
1/3	= 	0.(3)
1/4	= 	0.25
1/5	= 	0.2
1/6	= 	0.1(6)
1/7	= 	0.(142857)
1/8	= 	0.125
1/9	= 	0.(1)
1/10	= 	0.1
Where 0.1(6) means 0.166666..., and has a 1-digit recurring cycle. It can be seen that 1/7 has a 6-digit recurring cycle.

Find the value of d < 1000 for which 1/d contains the longest recurring cycle in its decimal fraction part.
*/

const until = 1000;

function find(s) {
    let pattern = "";
    for (let i = 0; i < s.length; i++) { // starting position.
        for (let j = 2; j < s.length; j++) { // size of substr to look for
            const ss = s.substr(i, j);
            let found = false;
            for (k = i + j; true; k += j) { // finding remaining matches
                const ss_k = s.substr(k, j);
                if (ss_k.length < ss.length) {
                    break; // substring ended
                }
                if (ss.indexOf(ss_k) === 0) {
                    found = true; // at least once it matched!
                }
                if (ss.indexOf(ss_k) !== 0) {
                    found = false;
                    break; // at least once it didn't match
                }
            }
            if (found) { // if we are this far, it means, we have a repeating pattern! try next
                if (ss.length > pattern.length) pattern = ss;
                break;
            }
        }
    }
    return pattern;
}

found = [];
for (let d = 2n; d <= until; d++) {
    const dec = 1n * 10n ** 2000n / d;
    const pattern = find(dec.toString());
    found.push({pattern,d,dec,l: pattern.length});
}

found.sort((a, b) => b.pattern.length - a.pattern.length);

console.log(found.slice(0, 3));
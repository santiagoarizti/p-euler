let prev1 = 1;
let prev2 = 1;
let sum = 0;
while (prev2 < 4*10**6) {
    [prev1, prev2] = [prev2, prev1 + prev2];
    if (prev2 % 2 === 0) sum += prev2;
}
console.log(sum);
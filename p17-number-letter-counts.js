// If the numbers 1 to 5 are written out in words: one, two, three, four, five, then there are 3 + 3 + 5 + 4 + 4 = 19 letters
//   used in total.
// If all the numbers from 1 to 1000 (one thousand) inclusive were written out in words, how many letters would be used?
// NOTE: Do not count spaces or hyphens. For example, 342 (three hundred and forty-two) contains 23 letters and 115 (one
//   hundred and fifteen) contains 20 letters. The use of "and" when writing out numbers is in compliance with British usage.
const n = parseInt(process.argv[2]) || 15; // 1000 -> 21124

const map = [];
map[1] = "one";
map[2] = "two";
map[3] = "three";
map[4] = "four";
map[5] = "five";
map[6] = "six";
map[7] = "seven";
map[8] = "eight";
map[9] = "nine";

map[10] = "ten";
map[20] = "twenty";
map[30] = "thirty";
map[40] = "forty";
map[50] = "fifty";
map[60] = "sixty";
map[70] = "seventy";
map[80] = "eighty";
map[90] = "ninety";

for (let i = 1; i < 10; i++) {
    map[i * 100] = map[i] + " hundred and";
    map[i * 1000] = map[i] + " thousand and";
}

const special = [
    [/\bten one$/, "eleven"],
    [/\bten two$/, "twelve"],
    [/\bten three$/, "thirteen"],
    [/\bten four$/, "fourteen"],
    [/\bten five$/, "fifteen"],
    [/\bten six$/, "sixteen"],
    [/\bten seven$/, "seventeen"],
    [/\bten eight$/, "eighteen"],
    [/\bten nine$/, "nineteen"],
    [/ and$/, ""],
];

function getNumberText(i) {
    let words = [];
    for (let j = 10; i > 0; j *= 10) {
        const r = i % j;
        if (r) {
            i -= r;
            words.push(map[r]);
        }
    }
    
    const word = words.reverse().join(" ");

    return special.reduce((w, [r, s]) => w.replace(r, s), word);
}

count = 0;
for (let i = 1; i <= n; i++) {
    const w = getNumberText(i);
    const l = w.replace(/ /g, "").length;
    console.log(w, l);
    count += l;
}
console.log(count);
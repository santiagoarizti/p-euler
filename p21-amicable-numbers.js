/*
Let d(n) be defined as the sum of proper divisors of n (numbers less than n which divide evenly into n).
If d(a) = b and d(b) = a, where a ≠ b, then a and b are an amicable pair and each of a and b are called amicable numbers.

For example, the proper divisors of 220 are 1, 2, 4, 5, 10, 11, 20, 22, 44, 55 and 110; therefore d(220) = 284.
The proper divisors of 284 are 1, 2, 4, 71 and 142; so d(284) = 220.

Evaluate the sum of all the amicable numbers under 10000.
*/

// finds all divisors (including 1, excluding n) of any given number
function d(n) {
    let sr_n = Math.sqrt(n);
    let map = new Map([[1, true]]);
    for (let i = 2; i <= sr_n; i++) {
        if (n % i === 0) {
            map.set(i, true);
            map.set(n / i, true);
        }
    }
    const f = Array.from(map.keys());
    //return f.sort((a, b) => a - b);

    return f.reduce((sum, i) => sum + i, 0);
}

// candidates are all numbers coupled with sum of divisors...
const candidates = new Map();
function logAmicable(i) {
    const di = d(i);
    const key = `${Math.min(i, di)}-${Math.max(i, di)}`;
    const cnt = candidates.get(key);
    if (cnt) cnt.match = true;
    else candidates.set(key, {match: false, nums: [i, di]});
}

for (let i = 1; i <= 10000; i++) {
    logAmicable(i);
}

// ...amicable are all candidates with more than 1 match.
const amicable = Array.from(candidates.values()).filter(cnt => cnt.match).map(cnt => cnt.nums);

console.log(amicable);
const sum = amicable.reduce((s, n) => s + n[0] + n[1], 0);

// I guess the sum is asked so that the result is a scalar value for the html form
console.log(sum);
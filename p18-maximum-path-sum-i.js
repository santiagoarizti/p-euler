const t0 = `
3
7 4
2 4 6
8 5 9 3
`.trim().split(`
`).map(l => l.split(" ").map(n => parseInt(n)));

const t = `
75
95 64
17 47 82
18 35 87 10
20 04 82 47 65
19 01 23 75 03 34
88 02 77 73 07 63 67
99 65 04 28 06 16 70 92
41 41 26 56 83 40 80 70 33
41 48 72 33 47 32 37 16 94 29
53 71 44 65 25 43 91 52 97 51 14
70 11 33 28 77 73 17 78 39 68 17 57
91 71 52 38 17 14 91 43 58 50 27 29 48
63 66 04 68 89 53 67 30 73 16 69 87 40 31
04 62 98 27 23 09 70 98 73 93 38 53 60 04 23
`.trim().split(`
`).map(l => l.split(" ").map(n => parseInt(n)));

// my thought was: on each row you must either remain in same pos, or advance 1.
// this is equivalent as having a binary number of same length as our triangle (-1, because on first row we don't make decision.)
// and each digit represents "stay" or "advance"
// so I tried all bitmasks, and kept the best one.

const bits = t.length - 1;
const maxBin = 2 ** bits;
const best = {path: null, sum: 0, bm: 0};
for (let i = 0; i < maxBin; i++) {
    let ii = 0;
    const path = [t[0][0]];
    for (let j = 0; j < bits; j++) {
        const bit = i & 2 ** j ? 1 : 0;
        ii += bit;
        path.push(t[j+1][ii]);
    }
    const s = path.reduce((s, p) => s + p, 0);
    if (s > best.sum) {
        best.path = path;
        best.sum = s;
        best.bm = i.toString(2);
    }
}
console.log(best);
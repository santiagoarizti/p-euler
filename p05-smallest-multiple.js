const num = 20;

// simple fnc, get all primes below a certain ceiling
function getPrimesBelow(n) {
    const p = [];
    for (let i = 2; i < n; i++) {
        if (isPrime(i)) p.push(i);
    }
    return p;
}

// is-prime function uses cache to avoid calculating double. it is recursive.
const checkedPrimes = [];
function isPrime(n) {
    if (checkedPrimes[n] !== undefined) {
        // if we calculated it before, reuse that result.
        return checkedPrimes[n];
    }
    
    let p = true;
    // because symetry of order of factors in product, we don't need to explore above sqrt
    const sqn = Math.sqrt(n);
    for (let i = Math.floor(sqn); i > 1; i--) {
        if (isPrime(i)) {
            if (n % i === 0) {
                p = false;
                break;
            }    
        }
    }

    checkedPrimes[n] = p;
    return p;
}

// using our primes, now we can explore minimum common multiple
const pb = getPrimesBelow(num);

// count how many times one of our base primes was used as factor
const counts = pb.map(p => 0);
for (let i = 2; i <= num; i++) {
    let ii = i; // make a copy, will divide it until 0
    for (let j = 0; j < pb.length; j++) {
        let cnt = 0; // how many times was this prime used as factor on this i?
        for (cnt = 0; ii % pb[j] === 0;) {
            ii = ii / pb[j];
            cnt++;
        }
        // bump the total for this prime
        counts[j] = Math.max(cnt, counts[j]);
    }
}

// now it is very simple, we just multiply each prime together by each time it was used (minimum common multiple)
const sum = counts.reduce((mlt, cnt, i) => mlt * pb[i] ** cnt, 1);

console.log(counts);
console.log(pb);
console.log(sum);
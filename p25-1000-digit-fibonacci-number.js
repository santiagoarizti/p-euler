/*
The Fibonacci sequence is defined by the recurrence relation:

Fn = Fn−1 + Fn−2, where F1 = 1 and F2 = 1.
Hence the first 12 terms will be:

F1 = 1
F2 = 1
F3 = 2
F4 = 3
F5 = 5
F6 = 8
F7 = 13
F8 = 21
F9 = 34
F10 = 55
F11 = 89
F12 = 144
The 12th term, F12, is the first term to contain three digits.

What is the index of the first term in the Fibonacci sequence to contain 1000 digits?
*/

const l = parseInt(process.argv[2]) || 3;

function *fibonacci() {
    let f1 = 1n;
    let f2 = 1n;
    let i = 2;
    while (true) {
        [f1, f2] = [f2, f1 + f2];
        yield [f2, ++i];
    }
}

const f = fibonacci();
while (true) {
    const [fn, n] = f.next().value;
    if (fn.toString().length >= l) {
        console.log(fn, n);
        break;
    }
}
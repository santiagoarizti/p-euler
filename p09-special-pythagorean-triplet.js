num = 1000;

for (let a = 1; a <= num - 2; a++) {
    for (let b = 1; b <= num - a - 1; b++) {
        const c = num - a - b;
        if (a ** 2 + b ** 2 === c ** 2) {
            console.log({a, b, c, s: a+b+c, m: a*b*c});
        }
    }
}
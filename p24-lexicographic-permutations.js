/*
A permutation is an ordered arrangement of objects. For example, 3124 is one possible permutation of the digits 1, 2, 3 and 4.
If all of the permutations are listed numerically or alphabetically, we call it lexicographic order. The lexicographic permutations
of 0, 1 and 2 are:

012   021   102   120   201   210

What is the millionth lexicographic permutation of the digits 0, 1, 2, 3, 4, 5, 6, 7, 8 and 9?
*/
const options = process.argv[2] || "012";
const opt = options.split("");

// need to recurse to build smaller and smaller version each time until options = 1
const permutations = opt => opt.reduce((p, _, i) => {
    // first clone the options
    const slice = [...opt];
    // now pull one of the members (making slice smaller than options)
    const opt_i = slice.splice(i, 1)[0];
    // if slice is large enough, recurse until smallest slice, then append current i
    const sub_p = slice.length === 1 ? slice : permutations(slice); // array of strings
    // remember to add current i to all permutations found here
    return p.concat(sub_p.map(sp => opt_i + sp));
}, []);

const p = permutations(opt);
console.log(p.length, p[1000000-1]); // when opt = 0123456789 then millionth is 2783915460 (2.78 user)
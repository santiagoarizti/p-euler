//const num = 10n; // expecting 27
const num = 100n; // expecting ? 648

function factorial(n) {
    return n <= 1n ? 1n : n * factorial(n - 1n);
}

const f = factorial(num);

const s = f.toString()

const sum = s.split("").reduce((sum, s) => sum + parseInt(s), 0)

console.log(s, sum);
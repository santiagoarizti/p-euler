const below = 1e6;
let longest = 0;
let bestN = 1;

class CollatzGenerator {
    constructor() {
        this.known = [];
    }

    calculate(n) {
        let c = 1;
        const orig = n;
        while (n > 1) {
            if (this.known[n] !== undefined) {
                c += this.known[n] - 1;
                break;
            } else {
                n = n % 2 ? 3 * n + 1 /* odd */ : n / 2 /* even */;
                c++;
            }
        }
        this.known[orig] = c;
        return c;
    }
}

const gen = new CollatzGenerator();

for (let i = 1; i <= below; i++) {
    const curr = gen.calculate(i);
    if (curr > longest) {
        longest = curr;
        bestN = i;
    }
}

console.log(bestN);
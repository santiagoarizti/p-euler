const num = 100;

let sumNormal = 0;
let sumOfSquares = 0;
for (let i = 1; i <= num; i++) {
    sumNormal += i;
    sumOfSquares += i ** 2;
}

console.log(sumNormal ** 2 - sumOfSquares);
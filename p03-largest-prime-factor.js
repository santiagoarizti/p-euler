//const num = 13195;
const num = 600851475143;

function getPrimeFactors(n) {
    const pb = getPrimesBelow(num);
    const f = getFactors(num);
    return f.filter(ff => pb.indexOf(ff) !== -1);
}

function getFactors(n) {
    const f = [];
    const sqn = Math.sqrt(n);
    for (let i = 2; i < sqn; i++) {
        if (n % i === 0) f.push(i, n / i);
    }
    return f;
}

function getPrimesBelow(n) {
    const p = [];
    const sqn = Math.sqrt(n);
    for (let i = 2; i < sqn; i++) {
        if (isPrime(i)) p.push(i);
    }
    return p;
}

const checkedPrimes = [];
function isPrime(n) {
    if (checkedPrimes[n] !== undefined) {
        return checkedPrimes[n];
    }
    
    let p = true;
    const sqn = Math.sqrt(n);
    for (let i = Math.floor(sqn); i > 1; i--) {
        if (isPrime(i)) {
            if (n % i === 0) {
                p = false;
                break;
            }    
        }
    }

    checkedPrimes[n] = p;
    return p;
}

const allPrimeFactors = getPrimeFactors(num);
console.log(allPrimeFactors[allPrimeFactors.length-1]);

const lim = 1000;

function isPalindrome(s) {
    let p = true;
    for (let i = 0; i < s.length / 2; i++) {
        const a = s[i], b = s[s.length - i - 1];
        if (a !== b) {
            p = false;
            break;
        }
    }
    return p;
}

best = 0;
for (let i = lim; i > 0; i--) for (let j = lim; j > 0; j--) {
    if (i * j > best && isPalindrome(""+(i * j))) {
        best = i * j;
    }
}
console.log(best);

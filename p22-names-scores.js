/*
Using names.txt (right click and 'Save Link/Target As...'), a 46K text file containing over five-thousand first names,
begin by sorting it into alphabetical order. Then working out the alphabetical value for each name, multiply this value
by its alphabetical position in the list to obtain a name score.

For example, when the list is sorted into alphabetical order, COLIN, which is worth 3 + 15 + 12 + 9 + 14 = 53, is the
938th name in the list. So, COLIN would obtain a score of 938 × 53 = 49714.

What is the total of all the name scores in the file?
*/
const fs = require('fs');
const path = require('path');

const fn = path.resolve(__dirname, './resources/p022_names.txt');

function alphaVal(s) {
    let val = 0;
    const floor = 64;
    for (let i = 0; i < s.length; i++) val += s.charCodeAt(i) - floor;
    return val;
}

fs.readFile(fn, 'utf8', function(err, contents) {

    const names = JSON.parse('['+contents+']');

    names.sort((a, b) => a > b ? 1 : (a < b ? -1 : 0));

    const grandTotal = names.reduce((gt, n, i) => gt + alphaVal(n) * (i + 1), 0);

    console.log(grandTotal);
});

const n = parseInt(process.argv[2]) || 20;

// ok this is starting to look like "combinations with repetition"
// see the last section here: https://www.mathsisfun.com/combinatorics/combinations-permutations.html

// 5 flavors of icecream, 3 scoops allowed. expecting 35
//console.log(rCn(3 + 5 - 1, 3));

function factorial(f) {
    return f <= 1 ? 1 : f * factorial(f - 1);
}
function rCn(n, r) {
    return Math.round(factorial(n) / (factorial(r) * factorial(n - r)), 0);
}

const r = n;
const paths = rCn(r + n, r);

console.log(n, paths);
const num = 2 * 10 ** 6;

// simple fnc, get all primes below a certain ceiling
function getPrimesBelow(n) {
    const p = [];
    for (let i = 2; i < n; i++) {
        if (isPrime(i)) p.push(i);
    }
    return p;
}

// is-prime function uses cache to avoid calculating double. it is recursive.
const checkedPrimes = [];
function isPrime(n) {
    if (checkedPrimes[n] !== undefined) {
        // if we calculated it before, reuse that result.
        return checkedPrimes[n];
    }
    
    let p = true;
    // because symetry of order of factors in product, we don't need to explore above sqrt
    const sqn = Math.sqrt(n);
    for (let i = Math.floor(sqn); i > 1; i--) {
        if (isPrime(i)) {
            if (n % i === 0) {
                p = false;
                break;
            }    
        }
    }

    checkedPrimes[n] = p;
    return p;
}

console.log(getPrimesBelow(num).reduce((s, i) => s + i, 0));
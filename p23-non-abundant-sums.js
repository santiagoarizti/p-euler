/*
A perfect number is a number for which the sum of its proper divisors is exactly equal to the number.
For example, the sum of the proper divisors of 28 would be 1 + 2 + 4 + 7 + 14 = 28, which means that
28 is a perfect number.

A number n is called deficient if the sum of its proper divisors is less than n and it is called
abundant if this sum exceeds n.

As 12 is the smallest abundant number, 1 + 2 + 3 + 4 + 6 = 16, the smallest number that can be
written as the sum of two abundant numbers is 24. By mathematical analysis, it can be shown that all
integers greater than 28123 can be written as the sum of two abundant numbers. However, this upper
limit cannot be reduced any further by analysis even though it is known that the greatest number that
cannot be expressed as the sum of two abundant numbers is less than this limit.

Find the sum of all the positive integers which cannot be written as the sum of two abundant numbers.
*/

const max = parseInt(process.argv[2]) || 28123;

// finds all proper divisors (including 1, excluding n) of any given number
function d(n) {
    let sr_n = Math.sqrt(n);
    let map = new Map([[1, true]]);
    for (let i = 2; i <= sr_n; i++) {
        if (n % i === 0) {
            map.set(i, true);
            map.set(n / i, true);
        }
    }
    const f = Array.from(map.keys());
    //return f.sort((a, b) => a - b);
    return f.reduce((sum, i) => sum + i, 0);
}

// first find all abundant numbers le 28123
const abundant = [];
for (let i = 1; i <= max; i++) {
    if (d(i) > i) {
        abundant.push(i);
    }
}

// contains all possible sums of abundant numbers
const can = abundant.reduce((m, i) => abundant.reduce((m, j) => m.set(i + j, 1), m), new Map());
const cannot = [];
let sum = 0;
for (let i = 0; i < max; i++) {
    if (!can.has(i)) {
        cannot.push(i);
        sum += i;
    }
} // 4179871 (took 2.2s)

console.log(cannot, sum);